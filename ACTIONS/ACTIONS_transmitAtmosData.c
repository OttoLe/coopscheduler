/*
 * ACTIONS_transmitAtmosData.c
 *
 *  Created on: Feb 18, 2021
 *      Author: Kirg
 */

#include "ACTIONS_transmitAtmosData.h"
#include "SCHEDULER/scheduler.h"
#include "UTILITY/stringManipulation.h"

static void ATMOSDATA_transmit(ptrTaskFunctionArg_t ptr){
	char buffer[200];
	char* bufFillPos = buffer;

	variableEntry_t* varEntryPtr =  SCHEDULER_VARLIST_getVarEntryPtr("Temperature");

	bufFillPos = SCHEDULER_VARLIST_varToString(varEntryPtr++, bufFillPos);
	memmove(bufFillPos-1, bufFillPos-2, 3);
	*(bufFillPos++-2)= '.';
	bufFillPos = strCpyWithPtr(bufFillPos, " degC  ");

	bufFillPos = SCHEDULER_VARLIST_varToString(varEntryPtr++, bufFillPos);
	memmove(bufFillPos-2, bufFillPos-3, 4);
	*(bufFillPos++-3)= '.';
	bufFillPos = strCpyWithPtr(bufFillPos, " kPa  ");

	bufFillPos = SCHEDULER_VARLIST_varToString(varEntryPtr++, bufFillPos);
	memmove(bufFillPos-2, bufFillPos-3, 4);
	*(bufFillPos++-3)= '.';
	bufFillPos = strCpyWithPtr(bufFillPos, " %rH   ");

	bufFillPos = SCHEDULER_VARLIST_varToString(varEntryPtr, bufFillPos);
	bufFillPos = strCpyWithPtr(bufFillPos, " Ohms\n");

	COMS_LOCAL_transmit(buffer, bufFillPos-buffer);
}

static task_t listAtmosData = {.taskName = "LAtmosData", .taskFunctionPtr = ATMOSDATA_transmit};

void ATMOSDATA_TRANSMISSION_init(){
	schedulableTaskPtrList[LAtmosData] = &listAtmosData;
	SCHEDULER_addRepeatTask(LAtmosData, 550, 5000);
}
