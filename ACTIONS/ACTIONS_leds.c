/*
 * ACTIONS_leds.c
 *
 *  Created on: Feb 2, 2021
 *      Author: Kirg
 */

#include "SCHEDULER/scheduler.h"

static task_t statusLedOn = {.taskName = "StatusLedOn", .taskFunctionPtr = LED_STATUS_set};
static task_t statusLedOff = {.taskName = "StatusLedOff", .taskFunctionPtr = LED_STATUS_reset};
static task_t statusLedToggle = {.taskName = "StatusLedTog", .taskFunctionPtr = LED_STATUS_toggle};
static task_t errLedOn = {.taskName = "ErrLedOn", .taskFunctionPtr = LED_ERR_set};
static task_t errLedOff = {.taskName = "ErrLedOff", .taskFunctionPtr = LED_ERR_reset};
static task_t errLedToggle = {.taskName = "ErrLedTog", .taskFunctionPtr = LED_ERR_toggle};

void LEDS_init(){
	schedulableTaskPtrList[StatusLedOn] = &statusLedOn;
	schedulableTaskPtrList[StatusLedOff] = &statusLedOff;
	schedulableTaskPtrList[StatusLedTog] = &statusLedToggle;
	schedulableTaskPtrList[ErrLedOn] = &errLedOn;
	schedulableTaskPtrList[ErrLedOff] = &errLedOff;
	schedulableTaskPtrList[ErrLedTog] = &errLedToggle;
}
