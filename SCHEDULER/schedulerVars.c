/*
 * schedulerVars.c
 *
 *  Created on: Feb 19, 2021
 *      Author: Kirg
 */

#include "schedulerVars.h"
#include "scheduler.h"

#include "../UTILITY/debug.h"
#include "../UTILITY/convert.h"
#include "../UTILITY/stringManipulation.h"

#define TO_STRING_BUF_SIZE 21

static int totalVars = 0;
variableEntry_t variableList[MAX_VARS] = {0};

static void SCHEDULER_listVariables(){
	char buffer[100];
	INFO("Variable List:");
	for(int i = 0; (variableList[i].variablePtr.ptr != 0) && (i < MAX_VARS) ; i++){
		strncpy(buffer, variableList[i].variableName, sizeof(buffer)-2);
		strcat(buffer," - ");
		SCHEDULER_VARLIST_varToString(&variableList[i], buffer + strlen(buffer));
		INFO(buffer);
	}
	INFO("End Variable List:\n");
}

static task_t listVariables = {.taskName = "ListVars", .taskFunctionPtr = SCHEDULER_listVariables};

void SCHEDULER_VARLIST_init(){
	schedulableTaskPtrList[ListVars] = &listVariables;
}

void SCHEDULER_VARLIST_addUInt64(char name[], uint64_t* varPtr){
	strncpy(variableList[totalVars].variableName, name, MAX_VAR_NAME_LENGTH-1);
	variableList[totalVars].variablePtr.ui64Ptr = varPtr;
	variableList[totalVars].variableType = ui64;
	totalVars++;
}

void SCHEDULER_VARLIST_addUInt32(char name[], uint32_t* varPtr){
	strncpy(variableList[totalVars].variableName, name, MAX_VAR_NAME_LENGTH-1);
	variableList[totalVars].variablePtr.ui32Ptr = varPtr;
	variableList[totalVars].variableType = ui32;
	totalVars++;
}

void SCHEDULER_VARLIST_addInt32(char name[], int32_t* varPtr){
	strncpy(variableList[totalVars].variableName, name, MAX_VAR_NAME_LENGTH-1);
	variableList[totalVars].variablePtr.i32Ptr = varPtr;
	variableList[totalVars].variableType = i32;
	totalVars++;
}

void SCHEDULER_VARLIST_addStr(char name[], char* strVar){
	strncpy(variableList[totalVars].variableName, name, MAX_VAR_NAME_LENGTH-1);
	variableList[totalVars].variablePtr.str = strVar;
	variableList[totalVars].variableType = str;
	totalVars++;
}

variableEntry_t* SCHEDULER_VARLIST_getVarEntryPtr(char* varName){
	for (int varPos = 0; varPos < totalVars; varPos++) {
		if(variableList[varPos].variableName[0] == varName[0]){
			for (int charPos = 1; charPos < MAX_VAR_NAME_LENGTH; charPos++) {
				if(variableList[varPos].variableName[charPos] != varName[charPos]){
					break;
				}else if(varName[charPos] == '\0'){
					return &variableList[varPos];
				}
			}
		}
	}
	return 0;
}

char* SCHEDULER_VARLIST_varToString(variableEntry_t* varEntryPtr, char* charArrToFill){
	switch (varEntryPtr->variableType) {
		case ui32:
			return itoa64(*varEntryPtr->variablePtr.ui32Ptr, charArrToFill, TO_STRING_BUF_SIZE, 10);
			break;
		case i32:
			return itoa64(*varEntryPtr->variablePtr.i32Ptr, charArrToFill, TO_STRING_BUF_SIZE ,10);
			break;
		case ui64:
			return itoa64(*varEntryPtr->variablePtr.ui64Ptr, charArrToFill, TO_STRING_BUF_SIZE, 10);
			break;
		case  str:
			return strCpyWithPtr(charArrToFill, varEntryPtr->variablePtr.str);
		default:
			return charArrToFill;
			break;
	}
}
