/*
 * scheduler.c
 *
 *  Created on: Feb 2, 2021
 *      Author: Kirg
 */

#include <stdlib.h>

#include "scheduler.h"
#include "../ACTIONS/ACTIONS_leds.h"
#include "../UTILITY/debug.h"

typedef struct scheduledTask{
	timerTime_t taskExecuteTime;
	task_t* taskPtr;
} scheduledTask_t;

scheduledTask_t schedularList[MAX_SCHEDULED_TASKS] = {0};
task_t* schedulableTaskPtrList[MAX_TASKS] = {0};
int numScheduledItems = 0;
static timerTime_t schedulerTimer = 0;
static char deviceName[20] = DEVICE_NAME;
static char deviceID[20] = DEVICE_ID;

timerTime_t SCHEDULER_getTime(){
	return schedulerTimer;
}

static void SCHEDULER_listTasks(){
	char buffer[100];
	INFO("Task List:");
	for(int i = 0; (schedulableTaskPtrList[i] != 0) && (i < MAX_TASKS); i++){
		strncpy(buffer, schedulableTaskPtrList[i]->taskName, sizeof(buffer)-2);
		INFO(buffer);
	}
	INFO("End Task List:\n");
}

static void SCHEDULER_listScheduluedTasks(){
	char buffer[100];
	char smallBuf[16];
	INFO("Scheduled Task List:");
	for(int i = 0; (schedularList[i].taskPtr != 0) && (i < MAX_SCHEDULED_TASKS); i++){
		itoa(schedularList[i].taskExecuteTime, buffer, 10);
		strcat(buffer, "  - ");
		strcat(buffer, schedularList[i].taskPtr->taskName);
		strcat(buffer, " (");
		itoa(schedularList[i].taskPtr->repeatInterval, smallBuf, 10);
		strcat(buffer, smallBuf);
		strcat(buffer, ")");
		INFO(buffer);
	}
	INFO("End Scheduled Task List:\n");
}

static ret_e SCHEDULER_scheduleTask(task_t* unscheduledTaskPtr, timerTime_t taskDelay){
	if(unscheduledTaskPtr->taskFunctionPtr == NULL){
		WARN("Empty task function");
		return RET_WARN_NULL_PTR;
	}

	if(taskDelay == 0 && unscheduledTaskPtr->repeatInterval == 0){
		unscheduledTaskPtr->taskFunctionPtr(unscheduledTaskPtr);
		return RET_OK;
	}

	if(numScheduledItems >= MAX_SCHEDULED_TASKS){
		ERR("Task list full");
		return RET_ERR_OVERFLOW;
	}

	timerTime_t executeTime = schedulerTimer + taskDelay;
	int taskInsertPosition = numScheduledItems;
	for(int i = 0; i<numScheduledItems; i++){
		if((schedularList[i].taskExecuteTime > executeTime)){
			taskInsertPosition = i;
			break;
		}
	}
	for(int i = numScheduledItems; i > taskInsertPosition; i--){
		schedularList[i] = schedularList[i-1];
	}
	schedularList[taskInsertPosition].taskExecuteTime = executeTime;
	schedularList[taskInsertPosition].taskPtr = unscheduledTaskPtr;
	numScheduledItems++;
	return RET_OK;
}

ret_e SCHEDULER_addTask(taskId_e taskId, timerTime_t taskDelay_ms){
	schedulableTaskPtrList[taskId]->repeatInterval = 0;
	return SCHEDULER_scheduleTask(schedulableTaskPtrList[taskId], taskDelay_ms);
}

ret_e SCHEDULER_addRepeatTask(taskId_e taskId, timerTime_t taskDelay_ms, timerTime_t repeatInterval_ms) {
	schedulableTaskPtrList[taskId]->repeatInterval = repeatInterval_ms;
	return SCHEDULER_scheduleTask(schedulableTaskPtrList[taskId], taskDelay_ms);
}

task_t hardTaskList[8] = {0};
int numHardTasks = 0;

ret_e SCHEDULER_hardAddRepeatTask(char* taskName,  ptrTaskFunction_t taskFunctPtr, timerTime_t taskDelay_ms, timerTime_t repeatInterval_ms){
	strncpy(hardTaskList[numHardTasks].taskName, taskName, MAX_TASK_NAME_LENGTH);
	hardTaskList[numHardTasks].taskFunctionPtr = taskFunctPtr;
	hardTaskList[numHardTasks].repeatInterval = repeatInterval_ms;
	return SCHEDULER_scheduleTask(&hardTaskList[numHardTasks], taskDelay_ms);
}

ret_e SCHEDULER_removeTaskOnce(taskId_e taskId){
	task_t* taskPtr = schedulableTaskPtrList[taskId];
	for(int i = 0; i < numScheduledItems; i++){
		if(taskPtr == schedularList[i].taskPtr){
			if(i == 0){
				schedularList[0].taskPtr->repeatInterval = 0;
				break;
			} else {
				for(int j = i; j < numScheduledItems-1; j++){
					schedularList[j] = schedularList[j+1];
				}
				memset(&schedularList[--numScheduledItems], 0, sizeof(schedularList[0]));
				break;
			}
		}
	}
	return RET_OK;
}

ret_e SCHEDULER_removeTask(taskId_e taskId){
	task_t* taskPtr = schedulableTaskPtrList[taskId];
	for(int i = 0; i < numScheduledItems; i++){
		if(taskPtr == schedularList[i].taskPtr){
			if(i == 0){
				schedularList[0].taskPtr->repeatInterval = 0;
			} else {
				for(int j = i; j < numScheduledItems-1; j++){
					schedularList[j] = schedularList[j+1];
				}
				memset(&schedularList[--numScheduledItems], 0, sizeof(schedularList[0]));
				i--;
			}
		}
	}
	return RET_OK;
}

ret_e SCHEDULER_clearSchedularList(){
	memset(schedularList, 0, sizeof(schedularList[0])*MAX_SCHEDULED_TASKS);
	numScheduledItems = 0;
	return RET_OK;
}

static task_t listTasks = {.taskName = "ListTasks", .taskFunctionPtr = SCHEDULER_listTasks};
static task_t listScheduluedTasks = {.taskName = "ListSchedule", .taskFunctionPtr = SCHEDULER_listScheduluedTasks};



ret_e SCHEDULER_init(){
	VERBOSE("Init started");
	SCHEDULER_VARLIST_init();
	SCHEDULER_VARLIST_addStr("DeviceName", deviceName);
	SCHEDULER_VARLIST_addStr("DeviceID", deviceID);
	SCHEDULER_VARLIST_addUInt32("Timer", &schedulerTimer); //TODO will need to switch to 64bit for long term operation
	schedulableTaskPtrList[ListTasks] = &listTasks;
	schedulableTaskPtrList[ListSchedule] = &listScheduluedTasks;
	LEDS_init();

	SCHEDULER_addRepeatTask(StatusLedTog, 0, 1000);
	INFO("Successful scheduler init\n\n");

	return RET_OK;
}

void SCHEDULER_run(){
	while(1){

		/* Mask the SysTick interrupt to prevent it changing part-way through
		 * scheduler operation and causing erroneous behavior.
		 * Then update the local timer value to be used in the scheduler
		 * and re-enable the SysTick interrupt. */
		HAL_NVIC_DisableIRQ(SysTick_IRQn); 	//TODO shift sysTick interrupt masking to a function
		schedulerTimer = uwTick;			//TODO uwtick is an STM specific variable, will need to be changed for portability to ther processors
		HAL_NVIC_EnableIRQ(SysTick_IRQn);

		/* Compare the taskExecuteTime of task at top of schedule list with timer and execute
	 	 * the task function if necessary, shifting the scheduler list accordingly */
		if((numScheduledItems != 0) && (schedularList[0].taskExecuteTime <= schedulerTimer)){
			schedularList[0].taskPtr->taskFunctionPtr(schedularList[0].taskPtr);
			/* Determines if the task needs to be rescheduled
			 * If repeatInterval is zero, task is not rescheduled, so remove it */
			if(schedularList[0].taskPtr->repeatInterval == 0){
				/*Shift all items in the list up by one and in so doing overwrite the topmost
				 *task that was just executed then clear the last entry */
				for(int i = 0; i < numScheduledItems-1; i++){
					schedularList[i] = schedularList[i+1];
				}
				memset(&schedularList[--numScheduledItems], 0, sizeof(schedularList[0]));
			} else { // Else the task needs to be rescheduled
				// Determine the position in the list at which the task needs to be rescheduled
				int taskInsertPosition = numScheduledItems-1;
				timerTime_t executeTimeForReschedule = schedularList[0].taskExecuteTime + schedularList[0].taskPtr->repeatInterval; //calculate the time at which to reschedule the event
				for(int i = 0; i<numScheduledItems; i++){
					if((schedularList[i].taskExecuteTime > executeTimeForReschedule)){
						taskInsertPosition = i-1;
						break;
					}
				}
				/* shift up all items in the list before the position at which the
				 * task is to be rescheduled down by one and reinserts
				 * the task in the list at the prescribed position */
				scheduledTask_t tempTaskForReschedule = schedularList[0]; //Create a temporary duplicate of the task to be rescheduled to avoid it being overwritten during the shift
				for(int i = 0; i < taskInsertPosition; i++){
					schedularList[i] = schedularList[i+1];
				}
				schedularList[taskInsertPosition].taskExecuteTime = executeTimeForReschedule;
				schedularList[taskInsertPosition].taskPtr = tempTaskForReschedule.taskPtr;
			}
		}
	}
}

/*TODO
 * build a safe mode to remove tasks that could be in an undesirable state
 */
