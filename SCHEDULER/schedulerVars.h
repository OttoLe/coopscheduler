/*
 * schedulerVars.h
 *
 *  Created on: Feb 19, 2021
 *      Author: Kirg
 */

#ifndef SRC_SCHEDULER_SCHEDULERVARS_H_
#define SRC_SCHEDULER_SCHEDULERVARS_H_

#define MAX_VAR_NAME_LENGTH 32
#define MAX_VARS 64

#include <inttypes.h>

typedef enum {
	ui32,
	i32,
	ui64,
	str
} varType_e;

typedef union{
	uint64_t* 	ui64Ptr;
	uint32_t* 	ui32Ptr;
	int32_t*  	i32Ptr;
	char* 		str;
	void* 		ptr;
} sharedVarPtr_t;

typedef struct variableEntry{
	char variableName[MAX_VAR_NAME_LENGTH];
	volatile sharedVarPtr_t variablePtr;
	varType_e variableType;
} variableEntry_t;

extern variableEntry_t variableList[MAX_VARS];

void SCHEDULER_VARLIST_init();
void SCHEDULER_VARLIST_addUInt64(char name[], uint64_t* varPtr);
void SCHEDULER_VARLIST_addUInt32(char name[], uint32_t* varPtr);
void SCHEDULER_VARLIST_addInt32(char name[], int32_t* varPtr);
void SCHEDULER_VARLIST_addStr(char name[], char* strVar);
variableEntry_t* SCHEDULER_VARLIST_getVarEntryPtr(char* varName);
char* SCHEDULER_VARLIST_varToString(variableEntry_t* varEntryPtr, char* charArr);


#endif /* SRC_SCHEDULER_SCHEDULERVARS_H_ */
