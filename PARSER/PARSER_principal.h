/*
 * PARSER_principal.h
 *
 *  Created on: Feb 16, 2021
 *      Author: Kirg
 */

#ifndef SRC_PARSER_PARSER_PRINCIPAL_H_
#define SRC_PARSER_PARSER_PRINCIPAL_H_

void PARSER_principalParse(uint8_t *instruction);

#endif /* SRC_PARSER_PARSER_PRINCIPAL_H_ */
