/*
 * stringManipulation.h
 *
 *  Created on: Feb 22, 2021
 *      Author: Kirg
 */

#ifndef SRC_UTILITY_STRINGMANIPULATION_H_
#define SRC_UTILITY_STRINGMANIPULATION_H_

#include <string.h>

static inline char* strCatWithPtr(char* dest, char* src){
	strcat(dest, src);
	return dest + strlen(dest);
}

static inline char* strCpyWithPtr(char* dest, char* src){
	strcpy(dest, src);
	return dest + strlen(src);
}

#endif /* SRC_UTILITY_STRINGMANIPULATION_H_ */
