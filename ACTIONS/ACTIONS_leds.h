/*
 * ACTIONS_leds.h
 *
 *  Created on: Feb 2, 2021
 *      Author: Kirg
 */

#ifndef INC_ACTIONS_GPIO_INC_ACTIONS_LEDS_H_
#define INC_ACTIONS_GPIO_INC_ACTIONS_LEDS_H_

void LEDS_init();

#endif /* INC_ACTIONS_GPIO_INC_ACTIONS_LEDS_H_ */
