/*
 * SENSOR_BME680.c
 *
 *  Created on: Feb 11, 2021
 *      Author: Kirg
 */

#include "SENSOR_BME680.h"
#include "../SCHEDULER/scheduler.h"
#include "../UTILITY/debug.h"

int32_t temperature = 0;

static int8_t BME680_I2C_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len){
	int8_t rslt = 0;
	uint8_t txData[len+1];
	txData[0] = reg_addr;
	memcpy(&txData[1], data, len);
	rslt = I2C_BME680_transmit((dev_id<<1), txData, len+1);
	return rslt;
}

static int8_t BME680_I2C_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len){
	int8_t rslt = 0;
	rslt = I2C_BME680_transmit((dev_id<<1), &reg_addr, 1);
	if (rslt) {return rslt;}
	rslt = I2C_BME680_receive((dev_id<<1), data, len);
	return rslt;
}

static struct bme680_dev atmospheric_sensor= {.dev_id = BME680_I2C_ADDR_PRIMARY, .intf = BME680_I2C_INTF, .read = BME680_I2C_read, .write = BME680_I2C_write, .delay_ms = HAL_Delay, .amb_temp = 25};

int8_t SENSOR_BME680_getData(struct bme680_field_data *atmosData){
	int8_t rslt;

	rslt = bme680_get_sensor_data(atmosData, &atmospheric_sensor);

	/* Trigger the next measurement if you would like to read data out continuously */
	if (atmospheric_sensor.power_mode == BME680_FORCED_MODE) {
		rslt = bme680_set_sensor_mode(&atmospheric_sensor);
	}
	return rslt;
}

static struct bme680_field_data atmosData = {0};


static void SENSOR_BME680_updateData(ptrTaskFunctionArg_t ptr){

	SENSOR_BME680_getData(&atmosData);

	temperature = atmosData.temperature;

	if(!(atmosData.status & BME680_GASM_VALID_MSK)){
		atmosData.gas_resistance = 0;
	}
}

static task_t measureAtmosData = {.taskName = "MAtmosData", .taskFunctionPtr = SENSOR_BME680_updateData};

int8_t SENSOR_BME680_init(){
	int8_t rslt = BME680_OK;
	rslt = bme680_init(&atmospheric_sensor);
	uint8_t set_required_settings;

	/* Set the temperature, pressure and humidity settings */
	atmospheric_sensor.tph_sett.os_hum = BME680_OS_2X;
	atmospheric_sensor.tph_sett.os_pres = BME680_OS_4X;
	atmospheric_sensor.tph_sett.os_temp = BME680_OS_8X;
	atmospheric_sensor.tph_sett.filter = BME680_FILTER_SIZE_3;

	/* Set the remaining gas sensor settings and link the heating profile */
	atmospheric_sensor.gas_sett.run_gas = BME680_ENABLE_GAS_MEAS;
	/* Create a ramp heat waveform in 3 steps */
	atmospheric_sensor.gas_sett.heatr_temp = 320; /* degree Celsius */
	atmospheric_sensor.gas_sett.heatr_dur = 150; /* milliseconds */

	/* Select the power mode */
	/* Must be set before writing the sensor configuration */
	atmospheric_sensor.power_mode = BME680_FORCED_MODE;

	/* Set the required sensor settings needed */
	set_required_settings = BME680_OST_SEL | BME680_OSP_SEL | BME680_OSH_SEL | BME680_FILTER_SEL
		| BME680_GAS_SENSOR_SEL;

	/* Set the desired sensor configuration */
	rslt = bme680_set_sensor_settings(set_required_settings,&atmospheric_sensor);

	/* Set the power mode */
	rslt = bme680_set_sensor_mode(&atmospheric_sensor);

	/* Get the total measurement duration */
	uint16_t meas_period;
	bme680_get_profile_dur(&meas_period, &atmospheric_sensor);

	VERBOSE("BME680 burn off started");

	/* Warm up run, loop through the measurement cycle 30 times to improve measurement quality later*/
	for (int i = 0; i < 30; ++i) {
		HAL_Delay(meas_period); /* Delay till the measurement is complete */
		rslt = bme680_set_sensor_mode(&atmospheric_sensor);
	}

	SCHEDULER_VARLIST_addInt32("Temperature", &temperature);
	SCHEDULER_VARLIST_addUInt32("Pressure", &atmosData.pressure);
	SCHEDULER_VARLIST_addUInt32("Humidity", &atmosData.humidity);
	SCHEDULER_VARLIST_addUInt32("GasRes", &atmosData.gas_resistance);

	schedulableTaskPtrList[MAtmosData] = &measureAtmosData;
	SCHEDULER_addRepeatTask(MAtmosData, 500, 2500);

	VERBOSE("BME680 burn off and init complete");

	return rslt;
}
