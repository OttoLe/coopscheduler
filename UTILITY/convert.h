/*
 * convert.h
 *
 *  Created on: Feb 22, 2021
 *      Author: Kirg
 */

#ifndef SRC_UTILITY_CONVERT_H_
#define SRC_UTILITY_CONVERT_H_

#include <inttypes.h>

void flipStr(char* str, int start, int end);
char* itoa64(int64_t input, char* arr, int arrLen, int base);

#endif /* SRC_UTILITY_CONVERT_H_ */
