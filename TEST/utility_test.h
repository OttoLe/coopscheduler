/*
 * utility_test.h
 *
 *  Created on: Feb 22, 2021
 *      Author: Kirg
 */

#ifndef SRC_TEST_UTILITY_TEST_H_
#define SRC_TEST_UTILITY_TEST_H_

void convertTest1();
void convertTest2();

#endif /* SRC_TEST_UTILITY_TEST_H_ */
