/*
 * scheduler.h
 *
 *  Created on: Feb 2, 2021
 *      Author: Kirg
 */

#ifndef INC_SCHEDULER_H_
#define INC_SCHEDULER_H_

#include "config.h"
#include "schedulerVars.h"

#define MAX_SCHEDULED_TASKS 32
#define MAX_TASKS 32
#define MAX_TASK_NAME_LENGTH 32


typedef enum {
	ListVars,
	ListTasks,
	ListSchedule,
	MAtmosData,
	LAtmosData,
	StatusLedOn,
	StatusLedOff,
	StatusLedTog,
	ErrLedOn,
	ErrLedOff,
	ErrLedTog,
	ADDITIONAL_TASKS
} taskId_e;

typedef enum {
	RET_OK = 0,
	RET_ERR_ARR_LEN,
	RET_ERR_INVALID_PARAM,
	RET_ERR_OVERFLOW,
	RET_ERR_COMMS,
	RET_ERR_POWER_FAIL,
	RET_WARN_NULL_PTR
} ret_e;

typedef uint32_t timerTime_t;
typedef uint32_t interval_t;
typedef uint32_t sharedVariable_t;


typedef struct task task_t;   // predefine structure
typedef task_t* ptrTaskFunctionArg_t;
typedef void (*ptrTaskFunction_t)(ptrTaskFunctionArg_t);

typedef struct task{
	char taskName[MAX_TASK_NAME_LENGTH];
	ptrTaskFunction_t taskFunctionPtr;
	interval_t repeatInterval;
	sharedVarPtr_t watchVariablePtr;
	sharedVariable_t constantCompareVariable;
	ptrTaskFunction_t passFunctionPtr;
	ptrTaskFunction_t failFunctionPtr;
} task_t;

extern task_t* schedulableTaskPtrList[MAX_TASKS];

ret_e SCHEDULER_init();
void SCHEDULER_run();
ret_e SCHEDULER_removeTask(taskId_e taskId);
ret_e SCHEDULER_clearSchedularList();
ret_e SCHEDULER_addTask(taskId_e taskId, timerTime_t taskDelay_ms);
ret_e SCHEDULER_addRepeatTask(taskId_e taskId  , timerTime_t taskDelay_ms,timerTime_t repeatInterval_ms);
timerTime_t SCHEDULER_getTime();

#endif /* INC_SCHEDULER_H_ */
