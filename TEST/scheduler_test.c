/*
 * scheduler_test.c
 *
 *  Created on: Feb 4, 2021
 *      Author: Kirg
 */

#include "SCHEDULER/scheduler.h"
#include "UTILITY/debug.h"

void scheduleTest0(){
	SCHEDULER_addRepeatTask(StatusLedTog, 0, 1000);
}

void scheduleTest7(){
	DBG("Starting test 7");
	SCHEDULER_addTask(TestT, 2000);
}

static void testFnctn(ptrTaskFunctionArg_t ptr){
	DBG("schedule list at start of task:");
	SCHEDULER_addTask(ListSchedule, 0);
	DBG("Removing this task");
	SCHEDULER_removeTask(TestT);
	DBG("schedule list after removal:");
	SCHEDULER_addTask(ListSchedule, 0);
	DBG("schedule list will be displayed in the next task:");
	SCHEDULER_addTask(ListSchedule, 1);
}

static task_t testTask = {.taskName = "TestTask", .taskFunctionPtr = testFnctn};

void TEST_init(){
	schedulableTaskPtrList[TestT] = &testTask;
	scheduleTest7();
}

#ifdef STM32F401xC

//void scheduleTest1(){
//	//schedule four tasks that will make the green LED turn on and off twice
//	SCHEDULER_addTask(GLedOn, 0);
//	SCHEDULER_addTask(GLedOff, 2500);
//	SCHEDULER_addTask(GLedOn, 5000);
//	SCHEDULER_addTask(GLedOff, 7500);
//}
//
//void scheduleTest2(){
//	//schedule 4 cyclical LED toggle tasks, simply toggle all the LEDS in a circle continuously
//	SCHEDULER_addRepeatTask(GLedTog, 0, 2000);
//	SCHEDULER_addRepeatTask(BLedTog, 500, 2000);
//	SCHEDULER_addRepeatTask(OLedTog, 1500, 2000);
//	SCHEDULER_addRepeatTask(RLedTog, 1000, 2000);
//}
//
//void scheduleTest3(){
//	//simply add LED toggle tasks to fill up the entire scheduler list
//	scheduleTest2();
//	for(int i = 0; i<(MAX_SCHEDULED_TASKS-4)/4; i++){
//		scheduleTest2();
//	}
//}
//
//void scheduleTest4(){
//	//test 2, but test that descheduling works
//	scheduleTest2();
//	SCHEDULER_removeTask(BLedTog);
//}
//
//void scheduleTest5(){
//	//print all the lists (variable, task, scheduler) and then add tasks and print the scheduler list again
//	SCHEDULER_addTask(ListVars, 0);
//	SCHEDULER_addTask(ListTasks, 0);
//	SCHEDULER_addTask(ListSchedule, 0);
//	scheduleTest2();
//	SCHEDULER_addTask(ListSchedule, 0);
//}
//
//void scheduleTest6(){
//	//do the same is in 5, but adjust the order so that one can see the list tasks on the printed schedule list
//	SCHEDULER_addTask(ListVars, 3);
//	SCHEDULER_addTask(ListTasks, 2);
//	SCHEDULER_addTask(ListSchedule, 1);
//	scheduleTest2();
//	SCHEDULER_addTask(ListSchedule, 4);
//}

#endif


//void scheduleTest1(){
//	SCHEDULER_addRepeatTask(RLedTog, 0, 500);
//}
//
//void scheduleTest2(){
//	SCHEDULER_addTask(ListSchedule, 0);
//	SCHEDULER_addRepeatTask(RLedTog, 0, 500);
//	SCHEDULER_addRepeatTask(GLedTog, 0, 1000);
//	for (int i = 0; i < 33; i++) {
//		SCHEDULER_addTask(RLedTog, 1000+(i*1000));
//	}
//	SCHEDULER_addTask(ListSchedule, 0);
//	SCHEDULER_removeTask(RLedTog);
//	SCHEDULER_addTask(ListSchedule, 0);
//}
//
//void scheduleTest4(){
//
//}




