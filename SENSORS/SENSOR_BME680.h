/*
 * SENSOR_BME680.h
 *
 *  Created on: Feb 11, 2021
 *      Author: Kirg
 */

#ifndef SRC_SENSORS_SENSOR_BME680_H_
#define SRC_SENSORS_SENSOR_BME680_H_


#include "BME680_driver-master/bme680.h"

int8_t SENSOR_BME680_init();
int8_t SENSOR_BME680_getData(struct bme680_field_data *atmosData);

#endif /* SRC_SENSORS_SENSOR_BME680_H_ */
