/*
 * convert.c
 *
 *  Created on: Feb 22, 2021
 *      Author: Kirg
 */


#include <inttypes.h>

void flipStr(char* str, int start, int end){
	while(start<end){
		char tmp = str[start];
		str[start++] = str[end];
		str[end--] = tmp;
	}
}

char* itoa64(int64_t input, char* charArr, int arrLen, int base){
	if (arrLen < 1){
		return charArr;
	}
	if(base>36 || base<2){
		charArr[0] = '\0';
		return charArr;
	}
	int pos = 0;
	int mod = 0;
	if(input >= 0){
		do{
			mod = input % base;
			if(mod < 10) charArr[pos++] = '0' + (mod);
			else charArr[pos++] = 'A' + (mod-10);
			input /= base;
		}while((input != 0) && (pos < (arrLen - 1)));
		flipStr(charArr, 0, pos-1);
	}else{
		charArr[pos++] = '-';
		do{
			mod = input % base;
			if(mod > -10) charArr[pos++] = '0' + (-mod);
			else charArr[pos++] = 'A' + (-mod-10);
			input /= base;
		}while((input != 0) && (pos < (arrLen - 1)));
		flipStr(charArr, 1, pos-1);
	}
	charArr[pos] = '\0';

	return charArr+pos;
}
