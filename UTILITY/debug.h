/*
 * debug.h
 *
 *  Created on: Feb 15, 2021
 *      Author: Kirg
 */

#ifndef SRC_UTILITY_DEBUG_H_
#define SRC_UTILITY_DEBUG_H_

#include <string.h>
#include "config.h"


typedef enum {
	L_VERBOSE	= 1,
	L_DBG		= 2,
	L_INFO		= 3,
	L_WARN		= 5,
	L_ERR		= 6,
	L_CRIT		= 7
}logLevel_e;

static inline void _log(logLevel_e level, char* msg){
	char buff[104];
	int len = strlen(msg);
	if(len > (sizeof(buff)-5))
		len = sizeof(buff)-5;
	switch (level) {
		case L_VERBOSE:
			buff[0] = 'V';
			break;
		case L_DBG:
			buff[0] = 'D';
			break;
		case L_INFO:
			buff[0] = 'I';
			break;
		case L_WARN:
			buff[0] = 'W';
			break;
		case L_ERR:
			buff[0] = 'E';
			break;
		case L_CRIT:
			buff[0] = 'C';
			break;
		default:
			buff[0] = ' ';
			break;
	}
	buff[1] = ':';
	buff[2] = ' ';
	memcpy(buff+3, msg, len);
	buff[len+3] = '\n';
	DEBUGGER_transmit(buff, len+4);
}

static inline void WARN(char* str){
	_log(L_WARN ,str);
}

static inline void ERR(char* str){
	_log(L_ERR ,str);
}

static inline void DBG(char* str){
	_log(L_DBG ,str);
}

static inline void INFO(char* str){
	_log(L_INFO ,str);
}

static inline void VERBOSE(char* str){
	_log(L_VERBOSE ,str);
}

#endif /* SRC_UTILITY_DEBUG_H_ */
