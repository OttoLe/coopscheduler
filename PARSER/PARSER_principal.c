/*
 * PARSER_principal.c
 *
 *  Created on: Feb 16, 2021
 *      Author: Kirg
 */

#include "../SCHEDULER/scheduler.h"
#include "../UTILITY/debug.h"
#include "../TEST/scheduler_test.h"

void PARSER_principalParse(uint8_t *instruction){
	int len = strlen((char*)instruction);
	if(len <= 1){
		switch (instruction[0]) {
		case 'v':
			SCHEDULER_addTask(ListVars, 0);
			break;
		case 't':
			SCHEDULER_addTask(ListTasks, 0);
			break;
		case 's':
			SCHEDULER_addTask(ListSchedule, 0);
			break;
		case 'a':
			SCHEDULER_addTask(ListVars, 0);
			SCHEDULER_addTask(ListTasks, 0);
			SCHEDULER_addTask(ListSchedule, 0);
			break;
		case 'r':
			scheduleTest0();
			break;
		default:
			ERR("Oops, not a valid input.");
			break;
		}
	}else{
		ERR("Oops, not a valid input.");
	}
}
