/*
 * utility_test.c
 *
 *  Created on: Feb 22, 2021
 *      Author: Kirg
 */

#include "../UTILITY/debug.h"
#include "../UTILITY/convert.h"

void convertTest1(){
	char arr[20];
	DBG("testing 9999999 10");
	itoa64(9999999, arr, 20, 10);
	DBG(arr);
	DBG("testing 0xfffff 16");
	itoa64(0xfffff, arr, 20, 16);
	DBG(arr);
	DBG("testing 0xfffff 32");
	itoa64(0xfffff, arr, 20, 32);
	DBG(arr);
	DBG("testing 0xfffff 2");
	itoa64(0xfffff, arr, 20, 2);
	DBG(arr);
	DBG("testing -0xfffff 8");
	itoa64(-0xfffff, arr, 20, 8);
	DBG(arr);
	DBG("testing -0xfffff 7");
	itoa64(-0xfffff, arr, 20, 7);
	DBG(arr);
	DBG("testing 0xffff 0");
	itoa64(0xffff, arr, 20, 0);
	DBG(arr);
	DBG("testing 0xffff 16 - 0 len arr");
	itoa64(0xffff, arr, 0, 16);
	DBG(arr);
	DBG("testing 0xffffffffffffff 10");
	itoa64(0xffffffffffffff, arr, 20, 10);
	DBG(arr);
	DBG("testing 0xffffffffffffffff 16");
	itoa64(0xffffffffffffffff, arr, 20, 16);
	DBG(arr);
	DBG("testing 0xffffffffffffffff 32");
	itoa64(0xffffffffffffffff, arr, 20, 32);
	DBG(arr);
	DBG("testing 0xffffffffffffffff 37");
	itoa64(0xffffffffffffffff, arr, 20, 37);
	DBG(arr);
	DBG("testing 0xfffffffffffffff 16 - arr len 8");
	itoa64(0xfffffffffffffff, arr, 8, 16);
	DBG(arr);
	DBG("testing -0xfffffff 16 - arr len 8");
	itoa64(-0xfffffff, arr, 8, 16);
	DBG(arr);
	DBG("testing -0xffffffff 16 - arr len 8");
	itoa64(-0xffffffff, arr, 8, 16);
	DBG(arr);
	DBG("testing -0xfffffffff 16 - arr len 8");
	itoa64(-0xfffffffff, arr, 8, 16);
	DBG(arr);
	DBG("testing tmp var 16");
	uint32_t tmp = 0xffffffff;
	itoa64(tmp, arr, 20, 16);
	DBG(arr);
	int32_t tmp2 = 0xffffffff;
	itoa64(tmp2, arr, 20, 16);
	DBG(arr);
}

void convertTest2(){
	char buffer[64] = {0};
	char* contPoint = buffer;
	DBG("Test strncpy");
	contPoint=strncpy(buffer, "TEST", 5);
	if(contPoint == buffer){
		DBG("same");
	}else if(contPoint == buffer+3){
		DBG("len");
	}else{
		DBG("neither");
	}
	DBG("Test strcat");
	contPoint=strcat(buffer, "TEST");
	if(contPoint == buffer){
		DBG("same");
	}else if(contPoint == buffer+3){
		DBG("len");
	}else{
		DBG("neither");
	}
}
